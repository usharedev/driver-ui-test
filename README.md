## Automated E2E UI Tests for the UShare Driver App

### 1 - Install Dependencies
`npm install`

### 2 - Download Appium Desktop
```
https://github.com/appium/appium-desktop/releases

Mac - brew cask install appium
```

### 4 - Run Appium app 
`appium`

#### To open an instance for each emulator you would like to test on, For Mac -
`open -n -a "appium"`


### 5 - Start emulators

### 6 - Run tests
Replace the apk file in the /tests directory with your test target apk

`npm run test`