var webdriverio = require('webdriverio');
const assert = require("assert");
require('dotenv').config()
const getDeviceInfo = require('./GetDeviceInfo')

describe('normal', function () {
  let user;
  let userInfo;
  const driverLocation = {latitude: -45.0349, longitude: 168.674, altitude: 0.00}
  const pickUpLocation = {latitude: -45.034862, longitude: 168.673806, altitude: 0.00}
  const destination = {latitude: -45.018434, longitude: 168.714359, altitude: 0.00}

  before(async function () {
    userInfo = getDeviceInfo(process.env.USER)

    user = await webdriverio.remote({
      port: userInfo.port,
      capabilities: {
        platformName: "Android",
        platformVersion: userInfo.platformVersion,
        deviceName: userInfo.deviceName,
        app: `${__dirname}/${userInfo.app}.apk`,
        appPackage: userInfo.appPackage,
        appActivity: ".MainActivity",
        autoGrantPermissions: "true",
        gpsEnabled: "true"
      }
    })
    await user.setGeoLocation(pickUpLocation)
  });

  it('should log in', async function () {
    await user.setImplicitTimeout(5000)

    const userEmail = userInfo.email
    const userPw = userInfo.pw

    const field = await user.$("~email")
    await field.setValue(userEmail)
    
    await user.hideKeyboard()
    
    const btn = await user.$("~getStartedBtn")
    await btn.click()
    
    await user.setImplicitTimeout(5000)
    
    const pw = await user.$("~pw")
    await pw.setValue(userPw)
    
    await user.hideKeyboard();
    
    const logInBtn = await user.$("~logInBtn")
    await logInBtn.click()
  });

  it('should navigate to current journey', async function () {
    if (userInfo.app === 'rider') this.skip()

    await user.setGeoLocation(driverLocation)

    const currentJourneyBtn = await user.$("~currentJourneyBtn")
    await currentJourneyBtn.click()
  });

  it('should navigate back to journey', async function () {
    if (userInfo.app === 'rider') this.skip()

    await user.pause(15000)

    const dashboardBtn = await user.$("~dashboardBtn")
    await dashboardBtn.click()
  });


  it('should start shift', async function () {
    if (userInfo.app === 'rider') this.skip()

    const shiftBtn = await user.$("~shiftBtn")
    await shiftBtn.click()

    const startShiftBtn = await user.$("~startShiftBtn")
    await startShiftBtn.click()

    const endShiftBtn = await user.$("~endShiftBtn")
    assert(endShiftBtn.isExisting(), true )
  });

  it('should accept trip request', async function () {
    if (userInfo.app === 'rider') this.skip()

    await user.setImplicitTimeout(15000)

    const acceptBtn = await user.$("~acceptBtn")
    await acceptBtn.click()

    const okayBtn = await user.$("id=android:id/button1")
    await okayBtn.click()
  });

  it('should contact rider', async function () {
    if (userInfo.app === 'rider') this.skip()

    await user.setImplicitTimeout(15000)

    const msgAlert = await user.$('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup')
    await msgAlert.click()

    const hideAlert = await user.$('~hideChatNotification')
    await hideAlert.click()

    const quickChat = await user.$('//android.view.ViewGroup[@content-desc="quickResponseBtn"]/android.widget.TextView')
    await quickChat.click()

    const chatBtn = await user.$('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[3]')
    await chatBtn.click()

    const mapBtn = await user.$('/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[3]/android.view.ViewGroup')
    await mapBtn.click()
  })


  it('should pick up rider', async function () {
    if (userInfo.app === 'rider') this.skip()

    await user.setGeoLocation(pickUpLocation)

    await user.pause(5000);

    const riderOnBoardBtn = await user.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]")
    await riderOnBoardBtn.click()
  });

  it('should go to destination and end the trip', async function () {
    if (userInfo.app === 'rider') this.skip()

    await user.pause(5000)

    await user.setGeoLocation(destination)

    await user.pause(10000)

    const endBtn = await user.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]")
    await endBtn.click()

    const endRideBtn = await user.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[3]")
    await endRideBtn.click()
  
  })

  it('should rate rider', async function () {
    if (userInfo.app === 'rider') this.skip()

    await user.setImplicitTimeout(10000)

    const star = await user.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.widget.TextView")
    await star.click()

    await user.pause(5000)

    const finishBtn = await user.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]")
    await finishBtn.click()
  })

  it('should end shift', async function () {
    if (userInfo.app === 'rider') this.skip()

    await user.setImplicitTimeout(10000)

    const shiftBtn = await user.$("~shiftBtn")
    await shiftBtn.click()

    const endShiftBtn = await user.$("~endShiftBtn")
    endShiftBtn.click()
  });


  it('should request a ride', async function () {
    if (userInfo.app === 'driver') this.skip()

    const inactiveSearchBar = await user.$("~inactiveSearchBar")
    await inactiveSearchBar.click()

    const destinationBar = await user.$("~destinationBar")
    await destinationBar.click()

    const field = await user.$("~addressInput")
    await field.setValue('Queenstown A')

    await user.hideKeyboard();

    const firstResult = await user.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]")
    await firstResult.click()
    await firstResult.click()

    const rideNowBtn = await user.$("~rideNowBtn")
    await rideNowBtn.click()

    await user.pause(15000)

    const confirmBtn = await user.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[5]")
    await confirmBtn.click()
  })

  it('should msg driver', async function () {
    if (userInfo.app === 'driver') this.skip()

    await user.pause(15000)

    await user.setImplicitTimeout(10000)

    const messageBtn = await user.$("~messageBtn")
    await messageBtn.click()

    const chatField = await user.$("~Talk to your driver")
    await chatField.setValue('Hi Driver!')

    await user.hideKeyboard();

    const sendBtn = await user.$("~send")
    await sendBtn.click()

    const mapBtn = await user.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[3]/android.view.ViewGroup")
    await mapBtn.click()
  })

  it('should rate driver', async function () {
    if (userInfo.app === 'driver') this.skip()

    await user.setImplicitTimeout(100000)

    const submitBtn = await user.$("~submitRatingBtn")
    await submitBtn.click()
  })
});