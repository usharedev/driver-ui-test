var webdriverio = require('webdriverio');
const assert = require("assert");
require('dotenv').config()
const getDeviceInfo = require('./GetDeviceInfo')

describe('Parallel', function () {
  let driver;
  let driverInfo;
  const location = {latitude: -45.018434, longitude: 168.714359, altitude: 0.00}

  before(async function () {
    driverInfo = getDeviceInfo(process.env.USER)

    driver = await webdriverio.remote({
      port: driverInfo.port,
      capabilities: {
        platformName: "Android",
        platformVersion: driverInfo.platformVersion,
        deviceName: driverInfo.deviceName,
        app: `${__dirname}/driver.apk`,
        appPackage: driverInfo.appPackage,
        appActivity: ".MainActivity",
        autoGrantPermissions: "true",
        gpsEnabled: "true"
      }
    })
    await driver.setGeoLocation(location)
  });

  it('should log in and accept location service', async function () {
    await driver.setImplicitTimeout(5000)

    const driverEmail = driverInfo.email
    const driverPw = driverInfo.pw

    const field = await driver.$("~email")
    await field.setValue(driverEmail)
    
    await driver.hideKeyboard()
    
    const btn = await driver.$("~getStartedBtn")
    await btn.click()
    
    await driver.setImplicitTimeout(5000)
    
    const pw = await driver.$("~pw")
    await pw.setValue(driverPw)
    
    await driver.hideKeyboard();
    
    const logInBtn = await driver.$("~logInBtn")
    await logInBtn.click()
  });

  it('should navigate to current journey', async function () {
    await driver.setGeoLocation(location)

    const currentJourneyBtn = await driver.$("~currentJourneyBtn")
    await currentJourneyBtn.click()
  });

  it('should navigate back to journey', async function () {
    await driver.pause(15000)

    const dashboardBtn = await driver.$("~dashboardBtn")
    await dashboardBtn.click()
  });


  it('should start shift', async function () {
    const shiftBtn = await driver.$("~shiftBtn")
    await shiftBtn.click()

    const startShiftBtn = await driver.$("~startShiftBtn")
    await startShiftBtn.click()

    const endShiftBtn = await driver.$("~endShiftBtn")
    assert(endShiftBtn.isExisting(), true )

    await driver.setGeoLocation(location)
  });

  it('should accept trip request', async function () {
    const acceptBtn = await driver.$("~acceptBtn")
    await acceptBtn.click()

    const okayBtn = await driver.$("id=android:id/button1")
    await okayBtn.click()
  });


  it('should pick up rider', async function () {
    const pickUpLocation = {latitude: -45.033349, longitude: 168.662906, altitude: 0.00}
    await driver.setGeoLocation(pickUpLocation)

    await driver.pause(5000);

    const riderOnBoardBtn = await driver.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]")
    await riderOnBoardBtn.click()
  });

  it('should go to destination and end the trip', async function () {
    const location1 = {latitude: -45.034862, longitude: 168.673806, altitude: 0.00}
    await driver.setGeoLocation(location1)

    await driver.pause(5000)

    const location2 = {latitude: -45.033702, longitude: 168.680994, altitude: 0.00}
    await driver.setGeoLocation(location2)

    await driver.pause(5000)

    const location3 = {latitude: -45.018434, longitude: 168.714359, altitude: 0.00}
    await driver.setGeoLocation(location3)

    await driver.pause(5000)

    const location4 = {latitude: -45.015870, longitude: 168.731568, altitude: 0.00}
    await driver.setGeoLocation(location4)

    await driver.pause(5000)


    const destination = {latitude: -45.022192, longitude: 168.738688, altitude: 0.00}
    await driver.setGeoLocation(destination)

    await driver.pause(5000)

    const endBtn = await driver.$("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]")
    await endBtn.click()

    const endRideBtn = await driver.$("~endRideBtn")
    await endRideBtn.click()
  
  })

  it('should end shift', async function () {
    const shiftBtn = await driver.$("~shiftBtn")
    await shiftBtn.click()

    const endShiftBtn = await driver.$("~endShiftBtn")
    endShiftBtn.click()
  });
});
