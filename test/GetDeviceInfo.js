const getDeviceInfo = (id) => {
  let deviceInfo = null
  switch (id) {
    case 'queenstown':
      deviceInfo = {
        email: "driver.queenstown@ushare.com",
        pw: "Mypassword1!",
        deviceName: "emulator-5556",
        platformVersion: "9",
        appPackage: "com.ushare.mobile.driver",
        app: "driver",
        port: 5001
      }
      break
    case 'rider':
      deviceInfo = {
        email: "rider7@ushare.com",
        pw: "Mypassword1!",
        deviceName: "emulator-5556",
        platformVersion: "9",
        appPackage: "com.ushare.mobile.rider",
        app: "rider",
        port: 5001
      }
      break
    default:
      deviceInfo = {
        email: 'marcus.driver@ushare.com',
        pw: 'Mypassword1!',
        deviceName: 'emulator-5554',
        platformVersion: '8',
        appPackage: "com.ushare.mobile.driver",
        app: "driver",
        port: 5000
      }
  }

  return deviceInfo
}

module.exports = getDeviceInfo